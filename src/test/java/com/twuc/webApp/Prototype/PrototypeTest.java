package com.twuc.webApp.Prototype;

import com.twuc.webApp.Prototype.SimplePrototypeScopeClass;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.*;

public class PrototypeTest {
    AnnotationConfigApplicationContext context;
    @BeforeEach
    private void initTestContext() {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp.Prototype");
    }

    @Test
    void should_create_two_SimplePrototypeScopeClass_to_test_Prototype() {
        SimplePrototypeScopeClass simpleOne = context.getBean(SimplePrototypeScopeClass.class);
        SimplePrototypeScopeClass simpleTwo = context.getBean(SimplePrototypeScopeClass.class);
        assertNotEquals(simpleOne, simpleTwo);
    }

    @Test
    void should_create_bean_when_scan_scope() {
        SimplePrototypeScopeClass simplePrototypeScopeClass = context.getBean(SimplePrototypeScopeClass.class);
        MyLogger bean = context.getBean(MyLogger.class);
        assertEquals("create the bean.", bean.getLogger().get(0));
    }

    @Test
    void should_create_prototype_bean_when_get_bean() {
        SimplePrototypeScopeClass simplePrototypeScopeClass = context.getBean(SimplePrototypeScopeClass.class);
        MyLogger bean = context.getBean(MyLogger.class);
        assertEquals(1, bean.getLogger().size());
        context.getBean(SimplePrototypeScopeClass.class);
        assertEquals(2, bean.getLogger().size());
    }

    @Test
    void should_lazy_load_singleton_bean() {
        SimplePrototypeScopeClass simplePrototypeScopeClass = context.getBean(SimplePrototypeScopeClass.class);
        MyLogger bean = context.getBean(MyLogger.class);
        assertEquals(1, bean.getLogger().size());
        LazyObject bean1 = context.getBean(LazyObject.class);
        LazyObject bean2 = context.getBean(LazyObject.class);
        assertSame(bean1, bean2);
        assertEquals(2, bean.getLogger().size());
    }

    @Test
    void should_create_one_singleton_and_prototype_when_dependent() {

        SingletonDependsOnPrototype bean = context.getBean(SingletonDependsOnPrototype.class);
        SingletonDependsOnPrototype bean1 = context.getBean(SingletonDependsOnPrototype.class);
        assertSame(bean, bean1);
        assertSame(bean.getPrototypeDependent(), bean1.getPrototypeDependent());
    }

    @Test
    void should_create_one_singleton_and_create_two_prototype() {
        PrototypeScopeDependsOnSingleton pBean = context.getBean(PrototypeScopeDependsOnSingleton.class);
        PrototypeScopeDependsOnSingleton pBean1 = context.getBean(PrototypeScopeDependsOnSingleton.class);
        assertSame(pBean.getSingletonDependent(), pBean1.getSingletonDependent());
        assertNotSame(pBean, pBean1);
    }

    @Test
    void should_create_different_prototype_instance() {
        SimplePrototypeScopeClass simplePrototypeScopeClass = context.getBean(SimplePrototypeScopeClass.class);
        MyLogger myLogger = context.getBean(MyLogger.class);
        assertEquals(1, myLogger.getLogger().size());

        SingletonDependsOnPrototypeProxy bean = context.getBean(SingletonDependsOnPrototypeProxy.class);
        bean.someMethod();
        assertEquals(2, myLogger.getLogger().size());
        SingletonDependsOnPrototypeProxy bean1 = context.getBean(SingletonDependsOnPrototypeProxy.class);
        bean1.someMethod();
        assertEquals(3, myLogger.getLogger().size());
    }

    @Test
    void should_create_different_prototype_instance_when_call_to_string() {
        SimplePrototypeScopeClass simplePrototypeScopeClass = context.getBean(SimplePrototypeScopeClass.class);
        MyLogger myLogger = context.getBean(MyLogger.class);
        assertEquals(1, myLogger.getLogger().size());

        SingletonDependsOnPrototypeProxy bean = context.getBean(SingletonDependsOnPrototypeProxy.class);
        bean.getPrototypeDependentWithProxy().toString();
        assertEquals(2, myLogger.getLogger().size());
        SingletonDependsOnPrototypeProxy bean1 = context.getBean(SingletonDependsOnPrototypeProxy.class);
        bean.getPrototypeDependentWithProxy().toString();
        assertEquals(3, myLogger.getLogger().size());
    }

    @Test
    void should_create_the_prototype_() {
        SingletonDependsOnPrototypeProxy bean = context.getBean(SingletonDependsOnPrototypeProxy.class);
        System.out.println(bean.getClass().getSuperclass().getName());
        assertNotEquals("com.twuc.webApp.Proxy.PrototypeDependentWithProxy", bean.getPrototypeDependentWithProxy().getClass().getName());
    }


}
