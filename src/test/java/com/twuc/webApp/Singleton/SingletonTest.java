package com.twuc.webApp.Singleton;

import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.*;

public class SingletonTest {

    @Test
    void should_test_interface_in_Singleton() {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext("com.twuc.webApp.Singleton");
        InterfaceOneImpl interfaceOneImple = context.getBean(InterfaceOneImpl.class);
        InterfaceOne interfaceOne = context.getBean(InterfaceOne.class);
        assertNotNull(interfaceOneImple);
        assertNotNull(interfaceOne);
        assertEquals(interfaceOneImple,interfaceOne);
    }

    @Test
    void should_test_extend_in_Singleton() {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext("com.twuc.webApp.Singleton");
        Animal animal = context.getBean(Animal.class);
        Dog dog = context.getBean(Dog.class);
        assertNotNull(animal);
        assertNotNull(dog);
        assertEquals(animal, dog);
    }

    @Test
    void should_test_abstract_in_Singleton() {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext("com.twuc.webApp.Singleton");
        DerivedClass derivedClass = context.getBean(DerivedClass.class);
        AbstractBaseClass abstractBaseClass = context.getBean(AbstractBaseClass.class);
        assertNotNull(derivedClass);
        assertNotNull(abstractBaseClass);
        assertEquals(derivedClass, abstractBaseClass);
    }
}
