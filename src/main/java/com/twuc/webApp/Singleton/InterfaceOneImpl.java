package com.twuc.webApp.Singleton;

import com.twuc.webApp.Prototype.MyLogger;
import org.springframework.stereotype.Component;

@Component
public class InterfaceOneImpl implements InterfaceOne{
    private MyLogger myLogger;

    public InterfaceOneImpl(MyLogger myLogger) {
        this.myLogger = myLogger;
        this.myLogger.getLogger().add("create the bean.");
    }

    public MyLogger getMyLogger() {
        return myLogger;
    }
}
