package com.twuc.webApp.Singleton;

import org.springframework.stereotype.Component;

@Component
public class DerivedClass extends AbstractBaseClass{
    @Override
    public void doSomething() {
        System.out.println("noting");
    }
}
