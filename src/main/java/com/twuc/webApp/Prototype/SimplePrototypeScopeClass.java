package com.twuc.webApp.Prototype;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class SimplePrototypeScopeClass {

    private MyLogger myLogger;

    public SimplePrototypeScopeClass(MyLogger myLogger) {
        this.myLogger = myLogger;
        this.myLogger.getLogger().add("create the bean.");
    }

    public MyLogger getMyLogger() {
        return myLogger;
    }

}
